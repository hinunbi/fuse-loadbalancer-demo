package com.shb.service;

import com.shb.Application;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.*;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = Application.class)
public class AnyLinkFailoverTest {

  @Autowired
  ProducerTemplate producer;

  @Test
  public void process() {
    String result = producer.requestBody("direct:anylink", "Hi", String.class);
    Assert.assertEquals("ERROR", result);
  }
}