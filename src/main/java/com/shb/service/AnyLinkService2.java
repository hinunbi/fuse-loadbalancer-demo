package com.shb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AnyLinkService2 {

  private static final Logger logger = LoggerFactory.getLogger(AnyLinkService2.class);


  @SuppressWarnings("unused")
  public Object process(Object in) throws Exception {
    logger.info("AnyLinkService2 called");
    throw new Exception("AnyLinkService2 Raise Exception intentionally at " + new Date().toString());
  }
}
