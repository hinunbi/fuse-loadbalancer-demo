package com.shb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AnyLinkService1 {

  private static final Logger logger = LoggerFactory.getLogger(AnyLinkService1.class);


  @SuppressWarnings("unused")
  public Object process(Object in) throws Exception {
    logger.info("AnyLinkService1 called");
    throw new Exception("AnyLinkService1 Raise Exception intentionally at " + new Date().toString());
  }
}
